export type Nullable<T> = T | null;
export type Optional<T> = T | undefined;
export type PartiallyRequiredBy<T, K extends keyof T> = Omit<T, K> & Required<Pick<T, K>>;
export type PartiallyOptionalBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;
export type KeyNameMap<T> = {
    readonly [name in keyof T]-?: name;
};

export interface IItemsOf<T> {
    items: T[];
}
